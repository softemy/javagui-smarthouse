package house;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class HouseCanvas extends Canvas {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8876101135126280104L;
	private BufferedImage image;
	
	public void changeImage(BufferedImage file){
		this.image = file;
	}
	
	public void paint(Graphics g){
		super.paint(g);
		g.drawImage(image, 0, 0, this);
	}
}
