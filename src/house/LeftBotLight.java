package house;

public class LeftBotLight extends Light {

	public LeftBotLight(HouseCanvas c, String on_file, String dim_file,
			String off_file) {
		super(c, on_file, dim_file, off_file);
	}

	public String toString(){
		return "Bottom left light";
	}
}
