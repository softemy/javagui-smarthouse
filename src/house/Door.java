package house;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Door implements Sensor{
	private String status = "unlocked";
	protected BufferedImage lock_image;
	protected BufferedImage unlock_image;
	protected int tryBreak = 0;
	protected HouseCanvas canvas;
	
	public Door(HouseCanvas c, String lock_file, String unlock_file){
		canvas = c;
		try {
			if (lock_file != null)
			lock_image = ImageIO.read(new FileInputStream(lock_file));
			if (unlock_file != null)
			unlock_image = ImageIO.read(new FileInputStream(unlock_file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.lock();
	}
	
	public HouseCanvas getCanvas(){
		return this.canvas;
	}
	
	public void toggle(){
		if (this.status == "locked"){
			this.status = "unlocked";
			this.tryBreak = 0;
			update_gui(this.unlock_image);
		} else {
			this.status = "locked";
			update_gui(this.lock_image);
		}
	}
	
	public void lock(){
		if (this.status == "unlocked"){
			this.status = "locked";
			update_gui(this.lock_image);
		}
	}
	
	public void unlock(){
		if (this.status == "locked"){
			this.status = "unlocked";
			this.tryBreak = 0;
			update_gui(this.unlock_image);
		}
	}
	
	public void update_gui(BufferedImage temp){
		this.canvas.changeImage(temp);
		this.canvas.repaint();
	}
	
	public String getStatus(){
		return this.status;
	}
	
	public void tryOpen(){
		if (this.status == "locked"){
			this.tryBreak++;
		} else {
			this.tryBreak = 0;
		}
	}
	
	public void reset(){
		this.tryBreak = 0;
	}

	@Override
	public void addSensorObserver(SensorObserver s) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeSensorObserver() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isTriggered() {
		return tryBreak >= 3;
	}
}
