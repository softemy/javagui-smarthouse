package house;

public interface SensorObserver{

	public void	sensorTriggered(String message);

}