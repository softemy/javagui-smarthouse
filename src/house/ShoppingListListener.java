package house;

import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class ShoppingListListener implements ListSelectionListener {
	private RemoteFrame gui;
	public ShoppingListListener(RemoteFrame h){
		this.gui = h;
	}
    public void valueChanged(ListSelectionEvent e) {
    	ListSelectionModel lsm = (ListSelectionModel)e.getSource();
    	
    	if (!lsm.isSelectionEmpty() && !e.getValueIsAdjusting()) {
            // Find out which indexes are selected.
            int minIndex = lsm.getMinSelectionIndex();
            int maxIndex = lsm.getMaxSelectionIndex();
            for (int i = minIndex; i <= maxIndex; i++) {
                if (lsm.isSelectedIndex(i)) {
                	System.out.println(i);
                	gui.turnBackItem(i);
                }
            }
        }
    }
}
