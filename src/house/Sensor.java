package house;

public interface Sensor{
	
	void addSensorObserver(SensorObserver s);
	void removeSensorObserver();
	boolean isTriggered();
	
}