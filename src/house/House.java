package house;

import java.util.ArrayList;
import java.util.List;

public class House {
	@SuppressWarnings("unused")
	private RemoteFrame gui;
	
	private List<Item> items = new ArrayList<Item>();
	private List<Door> doors = new ArrayList<Door>();
	private List<Light> lights = new ArrayList<Light>();
	private AlarmSystem alarm;
	
	// keep changing on operations
	private List<Item> shoppingList = new ArrayList<Item>();
	private double totalShoppingCost = 0;
	// TODO: operators on shopping list:
	// 3. view the list with total cost
	
	public House(){
		// ADD YOUR ITEMS HERE, YOUR ITEMS NAME SHOULD BE UNIQUE
		// eg: item1, item2  NOT item1, item1, item2
		// one item in the form of (name, cost)
		// eg: new Item("name", 10);
		for (int i=0; i<10; i++){
			this.items.add(new Item("Item" + i, i));
		}
		
		// INIT DOOR
		this.doors.add(new FrontDoor(new HouseCanvas(),
				"frontDoorLocked.jpg", 
				"frontDoorUnlocked.gif"));
		this.doors.add(new GarageDoor(new HouseCanvas(),
				"garageDoorLocked.gif", 
				"garageDoorUnlocked.gif"));
		this.lights.add(new LeftBotLight(new HouseCanvas(),
				"roomLeftBottom.gif",
				null,
				"roomLeftBottomOff.gif"
				));
		this.lights.add(new LeftTopLight(new HouseCanvas(),
				"roomLeftTop.gif",
				null,
				"roomLeftTopOff.gif"
				));
		this.lights.add(new RightBotLight(new HouseCanvas(),
				"roomRightBottom.gif",
				null,
				"roomRightBottomOff.gif"
				));
		this.lights.add(new RightTopLight(new HouseCanvas(),
				"roomRightTop.gif",
				"roomRightTopDim.gif",
				"roomRightTopOff.gif"
				));
		this.alarm = new AlarmSystem(new HouseCanvas(),
				"alarmLightOff.gif",
				"alarmLightOn.gif"
				);
		this.gui = new RemoteFrame(this);
		
	}
	
	public HouseCanvas findDoorEntity(String name){
		for (int i=0; i<this.doors.size(); i++){
			if (this.doors.get(i).getClass().getName().contains(name)){
				return this.doors.get(i).getCanvas();
			}
		}
		return null;
	}
	
	public HouseCanvas findLightEntity(String name){
		for (int i=0; i<this.lights.size(); i++){
			if (this.lights.get(i).getClass().getName().contains(name)){
				return this.lights.get(i).getCanvas();
			}
		}
		return null;
	}
	
	public AlarmSystem getAlarm(){
		return this.alarm;
	}
	
	public void lockOrUnlockDoor(String type){
		for (int i=0; i<this.doors.size(); i++){
			if (this.doors.get(i).getClass().getName().contains(type)){
				this.doors.get(i).toggle();
				break;
			}
		}
	}
	
	public void changeLightState(String type){
		for (int i=0; i<this.lights.size(); i++){
			if (this.lights.get(i).getClass().getName().contains(type)){
				this.lights.get(i).changeState();
				break;
			}
		}
	}
	
	public void turnOnLight(String type){
		for (int i=0; i<this.lights.size(); i++){
			if (this.lights.get(i).getClass().getName().contains(type)){
				this.lights.get(i).turnOn();
			}
		}
	}
	
	public void turnOffLight(String type){
		for (int i=0; i<this.lights.size(); i++){
			if (this.lights.get(i).getClass().getName().contains(type)){
				this.lights.get(i).turnOff();
			}
		}
	}
	
	public void lockDoor(String type){
		for (int i=0; i<this.doors.size(); i++){
			if (this.doors.get(i).getClass().getName().contains(type)){
				this.doors.get(i).lock();
				break;
			}
		}
	}
	
	public void unlockDoor(String type){
		for (int i=0; i<this.doors.size(); i++){
			if (this.doors.get(i).getClass().getName().contains(type)){
				this.doors.get(i).unlock();
				break;
			}
		}
	}
	
	public Sensor getSensor(String type){
		for (int i=0; i<this.doors.size(); i++){
			if (this.doors.get(i).getClass().getName().contains(type)){
				return this.doors.get(i);
			}
		}
		return null;
	}
	
	public String getDoorStatus(String type){
		for (int i=0; i<this.doors.size(); i++){
			if (this.doors.get(i).getClass().getName().contains(type)){
				return this.doors.get(i).getStatus();
			}
		}
		return "";
	}
	
	public String getDoorStatus(){
		String result="";
		for (int i=0; i<this.doors.size(); i++){
			result += this.doors.get(i) + "-Status: " + this.doors.get(i).getStatus() + "|";
		}
		return result;
	}
	
	public String getLightStatus(){
		String result="";
		for (int i=0; i<this.lights.size(); i++){
			result += this.lights.get(i) + "-Status: " + this.lights.get(i).getStatus() + "|";
		}
		return result;
	}
	
	public String getLightStatus(String type){
		for (int i=0; i<this.lights.size(); i++){
			if (this.lights.get(i).getClass().getName().contains(type)){
				return this.lights.get(i).getStatus();
			}
		}
		return "";
	}
	
	public List<Item> getItemList(){
		return this.items;
	}
	
	public List<Item> getShoppingList(){
		return this.shoppingList;
	}
	
	public void removeShoppingList(Item i){
		this.shoppingList.remove(i);
		this.totalShoppingCost -= i.getCost();
	}
	
	public void addShoppingList(Item i){
		this.shoppingList.add(i);
		i.buy();
		this.totalShoppingCost += i.getCost();
	}
	
	public void removeAllShoppingList(){
		this.shoppingList.clear();
		this.totalShoppingCost = 0;
	}
	
	public double getTotalCost(){
		return this.totalShoppingCost;
	}
	
	public static void main(String[] args){
		new House();
	}

	public Door getDoor(String type) {
		for (int i=0; i<this.doors.size(); i++){
			if (this.doors.get(i).getClass().getName().contains(type)){
				return this.doors.get(i);
			}
		}
		return null;
	}

	public HouseCanvas findAlarmEntity() {
		return this.alarm.getCanvas();
	}
}
