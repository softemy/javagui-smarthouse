package house;

public class FrontDoor extends Door {

	public FrontDoor(HouseCanvas c, String lock_file, String unlock_file) {
		super(c, lock_file, unlock_file);
	}
	
	public String toString(){
		return "Front door";
	}
}
