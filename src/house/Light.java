package house;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Light {
	private String id;
	private String status = "off"; // On Off Dim
	private boolean dimmable = false;
	private HouseCanvas canvas;
	private BufferedImage on;
	private BufferedImage off;
	private BufferedImage dim;
	
	public Light(HouseCanvas c, String on_file, String dim_file, String off_file){
		canvas = c;
		try {
			if (on_file != null)
			on = ImageIO.read(new FileInputStream(on_file));
			if (dim_file != null){
				dim = ImageIO.read(new FileInputStream(dim_file));
				dimmable = true;
			}
			if (off_file != null)
			off = ImageIO.read(new FileInputStream(off_file));
			System.out.println(on);
			System.out.println(dim);
			System.out.println(off);
			turnOn();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void changeState(){
		if (status=="On"){
			if (dimmable){
				status = "Dim";
				update_gui(this.dim);
			} else {
				status = "Off";
				update_gui(this.off);
			}
		} else if (status=="Dim") {
			status = "Off";
			update_gui(this.off);
		} else {
			status = "On";
			update_gui(this.on);
		}
	}
	
	public void turnOn(){
		status = "On";
		update_gui(this.on);
	}
	
	public void turnOff(){
		status = "Off";
		update_gui(this.off);
	}
	
	public void update_gui(BufferedImage image){
		this.canvas.changeImage(image);
		this.canvas.repaint();
	}
	
	public HouseCanvas getCanvas(){
		return this.canvas;
	}
	
	public String getStatus(){
		return this + " is " + status;
	}
	
	public String getId(){
		return this.id;
	}
}
