package house;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ButtonGroup;
import javax.swing.ListSelectionModel;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import java.awt.FlowLayout;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import java.awt.Button;

public class RemoteFrame extends JFrame {
	private House house;
	
	private static final long serialVersionUID = 3884116415824208166L;
	private JPanel contentPane;
	private JLabel totalCost;
	private JLabel message;
	private HouseCanvas canvas;
	private HouseCanvas garageDoorCanvas;
	private HouseCanvas frontDoorCanvas;
	private String selectedLight = "All";
	private int time = 1;
	//private JLabel shoppingLabel = new JLabel("Shopping list");
	//private JLabel housePicture = new JLabel("House picture");
	private ShoppingListGUI shoppingList;
	//private JLabel remainingLabel = new JLabel("Remaining items");
	private RemainingListGUI remainingList;
	private JTextField timeInput;
	private JCheckBox frontDoorCheckbox;
	private JCheckBox garageDoorCheckbox;
	

	/**
	 * Create the frame.
	 */
	public RemoteFrame(House h) {
		this.house = h;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 709, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		totalCost = new JLabel("Total cost: 0$");
		totalCost.setBounds(6, 390, 369, 16);
		contentPane.add(totalCost);
		
		message = new JLabel("Message:");
		message.setBounds(6, 6, 588, 16);
		contentPane.add(message);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(600, 28, 106, 356);
		contentPane.add(scrollPane);
		
		remainingList = new RemainingListGUI(this, this.house.getItemList(), 
				new DefaultListModel());
		scrollPane.setViewportView(remainingList);
		remainingList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);;
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(492, 28, 106, 356);
		contentPane.add(scrollPane_1);
		
		shoppingList = new ShoppingListGUI(this, this.house.getShoppingList(), 
				new DefaultListModel());
		scrollPane_1.setViewportView(shoppingList);
		shoppingList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		frontDoorCanvas = house.findDoorEntity("FrontDoor");
		frontDoorCanvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)){
					message.setText("Message: " + house.getDoorStatus("FrontDoor"));
					Door d = house.getDoor("FrontDoor");
					d.tryOpen();
					if (d.isTriggered()){
						house.getAlarm().alarm();
					}
				}
				else {
					house.lockOrUnlockDoor("FrontDoor");
					message.setText("Message: " + "Front door is " + house.getDoorStatus("FrontDoor"));
				}
			}
		});
		frontDoorCanvas.setBounds(262, 217, 38, 80);
		contentPane.add(frontDoorCanvas);
		
		garageDoorCanvas = house.findDoorEntity("GarageDoor");
		garageDoorCanvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (SwingUtilities.isLeftMouseButton(e)){
					message.setText("Message: " + house.getDoorStatus("GarageDoor"));
					Door d = house.getDoor("GarageDoor");
					d.tryOpen();
					if (d.isTriggered()){
						house.getAlarm().alarm();
					}
				}
				else {
					house.lockOrUnlockDoor("GarageDoor");
					message.setText("Message: " + "Garage door is " + house.getDoorStatus("GarageDoor"));
				}
			}
		});
		garageDoorCanvas.setBounds(30, 217, 93, 84);
		contentPane.add(garageDoorCanvas);
		
		canvas = new HouseCanvas();
		try {
			canvas.changeImage(ImageIO.read(new FileInputStream("house.jpg")));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HouseCanvas leftTopLight = this.house.findLightEntity("LeftTop");
		leftTopLight.setBounds(175, 133, 83, 84);
		leftTopLight.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if (SwingUtilities.isLeftMouseButton(e))
					message.setText("Message: " + house.getLightStatus("LeftTop"));
				else {
					house.changeLightState("LeftTop");
				}
			}
		});
		contentPane.add(leftTopLight);
		
		HouseCanvas leftBotLight = this.house.findLightEntity("LeftBot");
		leftBotLight.setBounds(124, 216, 136, 80);
		leftBotLight.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if (SwingUtilities.isLeftMouseButton(e))
					message.setText("Message: " + house.getLightStatus("LeftBot"));
				else {
					house.changeLightState("LeftBot");
				}
			}
		});
		contentPane.add(leftBotLight);
		
		HouseCanvas rightBotLight = this.house.findLightEntity("RightBot");
		rightBotLight.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if (SwingUtilities.isLeftMouseButton(e))
					message.setText("Message: " + house.getLightStatus("RightBot"));
				else {
					house.changeLightState("RightBot");
				}
			}
		});
		rightBotLight.setBounds(304, 218, 86, 80);
		contentPane.add(rightBotLight);
		
		HouseCanvas rightTopLight = this.house.findLightEntity("RightTop");
		rightTopLight.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if (SwingUtilities.isLeftMouseButton(e))
					message.setText("Message: " + house.getLightStatus("RightTop"));
				else {
					house.changeLightState("RightTop");
				}
			}
		});
		rightTopLight.setBounds(258, 133, 134, 84);
		contentPane.add(rightTopLight);
		
		HouseCanvas alarmCanvas = this.house.findAlarmEntity();
		alarmCanvas.setBounds(161, 28, 65, 79);
		
		contentPane.add(alarmCanvas);
		
		canvas.setBounds(6, 28, 480, 356);
		contentPane.add(canvas);
		
		
		JLabel shoppingListLabel = new JLabel("Shopping List");
		shoppingListLabel.setBounds(492, 390, 96, 16);
		contentPane.add(shoppingListLabel);
		
		JLabel remainingListLabel = new JLabel("Remaining Items");
		remainingListLabel.setBounds(600, 390, 115, 16);
		contentPane.add(remainingListLabel);
		
		JButton discardAll = new JButton("Discard All");
		
		discardAll.setBounds(502, 418, 96, 29);
		discardAll.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				for (int i=0; i < shoppingList.model.size(); i++){
					Item temp = (Item) shoppingList.model.getElementAt(i);
					remainingList.addItem(temp);
					house.removeShoppingList(temp);
				}
				shoppingList.removeAll();
				totalCost.setText("Total cost: " + house.getTotalCost() + "$");
				message.setText("Message: discard all shopping items");
			}
		});
		contentPane.add(discardAll);
		
		JButton buyAll = new JButton("Buy All");
		buyAll.setBounds(610, 418, 96, 29);
		buyAll.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				for (int i=0; i < remainingList.model.size(); i++){
					Item temp = (Item) remainingList.model.getElementAt(i);
					shoppingList.addItem(temp);
					house.addShoppingList(temp);
				}
				remainingList.removeAll();
				totalCost.setText("Total cost: " + house.getTotalCost() + "$");
				message.setText("Message: buy all remaining items");
			}
		});
		contentPane.add(buyAll);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(6, 443, 697, 129);
		contentPane.add(tabbedPane);
		
		JPanel doorRemote = new JPanel();
		tabbedPane.addTab("Door Controller", null, doorRemote, null);
		doorRemote.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JButton lockAllDoor = new JButton("Lock All Door");
		lockAllDoor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				house.lockDoor("FrontDoor");
				house.lockDoor("GarageDoor");
				message.setText("Message: " + house.getDoorStatus());
			}
		});
		lockAllDoor.setHorizontalAlignment(SwingConstants.LEFT);
		doorRemote.add(lockAllDoor);
		
		JButton unlockAllDoor = new JButton("Unlock All Doors");
		unlockAllDoor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				house.unlockDoor("FrontDoor");
				house.unlockDoor("GarageDoor");
				message.setText("Message: " + house.getDoorStatus());
			}
		});
		doorRemote.add(unlockAllDoor);
		
		JButton lockCheckStatus = new JButton("Check Door Status");
		lockCheckStatus.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				message.setText("Message: " + house.getDoorStatus());
			}
		});
		doorRemote.add(lockCheckStatus);
		
		JPanel lightRemote = new JPanel();
		tabbedPane.addTab("Light Controller", null, lightRemote, null);
		
		JButton onAllLights = new JButton("Turn On All Lights");
		onAllLights.setBounds(6, 5, 159, 29);
		onAllLights.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				house.turnOnLight("LeftTop");
				house.turnOnLight("LeftBot");
				house.turnOnLight("RightTop");
				house.turnOnLight("RightBot");			
			}
		});
		lightRemote.setLayout(null);
		lightRemote.add(onAllLights);
		
		JButton offAllLights = new JButton("Turn Off All Lights");
		offAllLights.setBounds(6, 48, 161, 29);
		offAllLights.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				house.turnOffLight("LeftTop");
				house.turnOffLight("LeftBot");
				house.turnOffLight("RightTop");
				house.turnOffLight("RightBot");			
			}
		});
		lightRemote.add(offAllLights);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(424, 13, 0, 12);
		separator.setForeground(Color.BLACK);
		lightRemote.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setForeground(Color.GRAY);
		separator_1.setBackground(Color.GRAY);
		separator_1.setBounds(162, 5, 10, 72);
		lightRemote.add(separator_1);
		
		JLabel lblToggleLightAfter = new JLabel("Toggle light after:");
		lblToggleLightAfter.setBounds(177, 12, 120, 16);
		lightRemote.add(lblToggleLightAfter);
		
		timeInput = new JTextField();
		timeInput.setText("1000");
		timeInput.setBounds(305, 6, 73, 28);
		lightRemote.add(timeInput);
		timeInput.setColumns(10);
		
		JLabel lblMs = new JLabel("(ms)");
		lblMs.setBounds(379, 10, 33, 16);
		lightRemote.add(lblMs);
		
		JRadioButton timerForAllLight = new JRadioButton("All lights");
		timerForAllLight.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectedLight = "All";
			}
		});
		timerForAllLight.setSelected(true);
		timerForAllLight.setBounds(334, 49, 90, 23);
		lightRemote.add(timerForAllLight);
		
		JRadioButton timerForLeftTop = new JRadioButton("Top left");
		timerForLeftTop.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectedLight = "LeftTop";
			}
		});
		timerForLeftTop.setBounds(436, 6, 90, 23);
		lightRemote.add(timerForLeftTop);
		
		JRadioButton timerForLeftBot = new JRadioButton("Bottom left");
		timerForLeftBot.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectedLight = "LeftBot";
			}
		});
		timerForLeftBot.setBounds(436, 49, 102, 23);
		lightRemote.add(timerForLeftBot);
		
		JRadioButton timerForRightBot = new JRadioButton("Bottom right");
		timerForRightBot.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectedLight = "RightBot";
			}
		});
		timerForRightBot.setBounds(550, 49, 120, 23);
		lightRemote.add(timerForRightBot);
		
		JRadioButton timerForRightTop = new JRadioButton("Top right");
		timerForRightTop.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				selectedLight = "RightTop";
			}
		});
		timerForRightTop.setBounds(550, 6, 120, 23);
		lightRemote.add(timerForRightTop);
		
		ButtonGroup group = new ButtonGroup();
		group.add(timerForAllLight);
		group.add(timerForLeftTop);
		group.add(timerForRightTop);
		group.add(timerForLeftBot);
		group.add(timerForRightBot);
		
		JButton timerRun = new JButton("Run");
		timerRun.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String userInput = timeInput.getText();
				try{
					time = Integer.parseInt(userInput);
					Timer counter = new Timer();
					counter.schedule(new TimerTask(){
						public void run(){
							changeLightState(selectedLight);
						}
					}, (long)time);
				} catch (NumberFormatException exception){
					message.setText("Error: Please input an integer!!!!");
				}
			}
		});
		
		timerRun.setBounds(179, 48, 57, 29);
		lightRemote.add(timerRun);
		
		JLabel lblFor = new JLabel("For -->");
		lblFor.setBounds(248, 53, 61, 16);
		lightRemote.add(lblFor);
		
		JPanel alarmRemote = new JPanel();
		tabbedPane.addTab("AlarmController", null, alarmRemote, null);
		alarmRemote.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("There are two sensors set to detect door breaks.");
		lblNewLabel.setVerticalAlignment(SwingConstants.TOP);
		lblNewLabel.setBounds(316, 6, 354, 16);
		alarmRemote.add(lblNewLabel);
		
		JLabel lblClickTimes = new JLabel("Click 3 times on a locked door to trigger a sensor.");
		lblClickTimes.setVerticalAlignment(SwingConstants.TOP);
		lblClickTimes.setBounds(316, 30, 354, 16);
		alarmRemote.add(lblClickTimes);
		
		JLabel lblNewLabel_1 = new JLabel("The alarm listens on:");
		lblNewLabel_1.setBounds(6, 6, 132, 16);
		alarmRemote.add(lblNewLabel_1);
		
		frontDoorCheckbox = new JCheckBox("Front Door Sensor");
		frontDoorCheckbox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (frontDoorCheckbox.isSelected())
					house.getAlarm().addSensor(house.getSensor("FrontDoor"));
				else
					house.getAlarm().removeSensor(house.getSensor("FrontDoor"));
				house.getDoor("FrontDoor").reset();
			}
		});
		frontDoorCheckbox.setBounds(6, 26, 183, 23);
		alarmRemote.add(frontDoorCheckbox);
		
		garageDoorCheckbox = new JCheckBox("Garage Door Sensor");
		garageDoorCheckbox.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (garageDoorCheckbox.isSelected())
					house.getAlarm().addSensor(house.getSensor("GarageDoor"));
				else
					house.getAlarm().removeSensor(house.getSensor("GarageDoor"));
				house.getDoor("GarageDoor").reset();
			}
		});
		garageDoorCheckbox.setBounds(6, 54, 183, 23);
		alarmRemote.add(garageDoorCheckbox);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setOrientation(SwingConstants.VERTICAL);
		separator_2.setForeground(Color.GRAY);
		separator_2.setBackground(Color.GRAY);
		separator_2.setBounds(294, 5, 10, 72);
		alarmRemote.add(separator_2);
		
		Button button = new Button("Stop");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				house.getAlarm().reset();
			}
		});
		button.setBounds(567, 49, 99, 28);
		alarmRemote.add(button);
		
		this.setVisible(true);
	}
	
	public void changeLightState(String type){
		if (type == "All"){
			house.changeLightState("LeftTop");
			house.changeLightState("LeftBot");
			house.changeLightState("RightTop");
			house.changeLightState("RightBot");
		} else {
			house.changeLightState(type);
		}
	}
	
	public void buyItem(int index){
		Item temp = (Item)this.remainingList.getSelectedValue();
		this.message.setText("Message: buy " + temp.getName());
		this.house.addShoppingList(temp);
		this.shoppingList.addItem(temp);
		this.remainingList.removeItem(temp);
		this.totalCost.setText("Total cost: " + this.house.getTotalCost() + "$");
	}
	
	public void turnBackItem(int index){
		Item temp = (Item)this.shoppingList.getSelectedValue();
		this.message.setText("Message: turn back " + temp.getName());
		this.house.removeShoppingList(temp);
		this.shoppingList.removeItem(temp);
		this.remainingList.addItem(temp);
		this.totalCost.setText("Total cost: " + this.house.getTotalCost() + "$");
	}
}
