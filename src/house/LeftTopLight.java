package house;

public class LeftTopLight extends Light {

	public LeftTopLight(HouseCanvas c, String on_file, String dim_file,
			String off_file) {
		super(c, on_file, dim_file, off_file);
	}

	public String toString(){
		return "Top left light";
	}
}
