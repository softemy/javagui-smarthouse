package house;

public class RightBotLight extends Light {

	public RightBotLight(HouseCanvas c, String on_file, String dim_file,
			String off_file) {
		super(c, on_file, dim_file, off_file);
	}

	public String toString(){
		return "Bottom right light";
	}
}
