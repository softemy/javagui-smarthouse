package house;

public class GarageDoor extends Door {

	public GarageDoor(HouseCanvas c, String lock_file, String unlock_file) {
		super(c, lock_file, unlock_file);
	}
	
	public String toString(){
		return "Garage door";
	}
}
