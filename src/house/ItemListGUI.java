package house;

import java.awt.Dimension;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JList;

public class ItemListGUI extends JList{
	public DefaultListModel model = new DefaultListModel();
	private static final long serialVersionUID = 1L;
	public ItemListGUI(List<Item> items, DefaultListModel model){
		super(model);
		this.model = model;
		for (Object i : items) this.model.addElement(i);
		this.setMaximumSize(new Dimension(100, 550));
		this.setMinimumSize(new Dimension(100, 550));
		
	}
	
	public void removeAll(){
		this.model.clear();
	}
	
	
	public void addItem(Item i){
		this.model.addElement(i);
	}
	
	public void removeItem(Item i){
		this.model.removeElement(i);
	}
}
