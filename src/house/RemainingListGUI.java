package house;

import java.awt.Color;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;

public class RemainingListGUI extends ItemListGUI{
	private static final long serialVersionUID = -3119211175842129248L;
	private RemoteFrame gui;
	public RemainingListGUI(RemoteFrame g, List<Item> items, DefaultListModel model) {
		super(items, model);
		this.gui = g;
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		this.getSelectionModel().addListSelectionListener(
				new RemainingListListener(this.gui));
	}
}
