package house;

import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

public class AlarmSystem implements SensorObserver {
	private List<Sensor> sensors = new ArrayList<Sensor>();
	private HouseCanvas canvas;
	private BufferedImage notAlarm;
	private BufferedImage alarm;
	
	public AlarmSystem(HouseCanvas c, String na, String a){
		canvas = c;
		try {
			if (na != null)
			notAlarm = ImageIO.read(new FileInputStream(na));
			if (a != null){
				alarm = ImageIO.read(new FileInputStream(a));
			}
			reset();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void sensorTriggered(String message) {
		
	}
	
	public void addSensor(Sensor s){
		this.sensors.add(s);
	}
	
	public void removeSensor(Sensor s){
		this.sensors.remove(s);
	}
	
	public void update_gui(BufferedImage image){
		this.canvas.changeImage(image);
		this.canvas.repaint();
	}

	public void alarm() {
		for (int i=0; i<sensors.size(); i++){
			if (sensors.get(i).isTriggered()){
				update_gui(this.alarm);
				break;
			}
		}
		
	}
	
	public void reset() {
		for (int i=0; i<sensors.size(); i++){
			Door temp = (Door)sensors.get(i);
			temp.reset();
		}
		update_gui(this.notAlarm);
	}

	public HouseCanvas getCanvas() {
		return canvas;
	}

}
