package house;

import java.awt.Color;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;

public class ShoppingListGUI extends ItemListGUI{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3836734261741257321L;
	private RemoteFrame gui;
	public ShoppingListGUI(RemoteFrame g, List<Item> items, DefaultListModel model) {
		super(items, model);
		this.gui = g;
		this.setBorder(BorderFactory.createLineBorder(Color.black));
		this.getSelectionModel().addListSelectionListener(
				new ShoppingListListener(this.gui));
	}
	
}
