package house;

public class Item {
	private double cost;
	private String name;
	private int quantity = 1;
	
	public Item(String name, double cost){
		this.name = name;
		this.cost = cost;
	}
	
	public String getName() {
		return this.name;
	}
	
	public boolean isAvailable(){
		return this.quantity > 0;
	}
	
	public void buy(){
		this.quantity--;
	}
	
	public double getCost() {
		return this.cost;
	}
	
	public String toString(){
		return this.name + "-" + this.cost + "$";
	}

}
